# Prueba Magento 2

### Descripci�n de la prueba
La prueba consiste en:

* Instalar Magento 2.3
* Instalar Sample Data
* Crear un m�dulo llamado Watts25/Evento
 	* Crear un evento para que cuando un usuario se suscriba al newsletter guarde en el log de Magento el email y la fecha de registro
	* El log debe llamarse de la siguiente manera para mantener coherencia con el nombre del m�dulo: __watts25_evento.log__
	
### Entrega
* Se deber� entregar todo el contenido dentro del repositorio actual en la rama __develop__ utilizando metodolog�a [gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
* Adjuntar la base de datos en .zip en __Downloads__ de Bitbucket